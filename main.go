package main

import (
	"flag"
	"fmt"
	"time"

	"bitbucket.org/writetosalman/go-water-lawn/config"
	"bitbucket.org/writetosalman/go-water-lawn/core/adhan"
	"bitbucket.org/writetosalman/go-water-lawn/core/garbagecollection"
	"bitbucket.org/writetosalman/go-water-lawn/core/water"
	"bitbucket.org/writetosalman/go-water-lawn/utility/dateutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
)

// main function
func main() {

	// Initialise .env file
	config.Initialise()

	// Declare config variable
	var appConfig config.Config
	appVersion := "1.1.4"

	// Save time
	timeNow := time.Now()

	// Get config
	appConfig, err := config.GetConfig()
	if err != nil {
		logutil.Log("Fatal error. " + err.Error())
		return
	}

	// Set config in dependencies
	logutil.LogLevel = appConfig.LogLevel
	adhan.AppConfig = &appConfig
	water.AppConfig = &appConfig
	garbagecollection.AppConfig = &appConfig

	// Grab command line arguments
	taskPtr := flag.String("task", "help", "-task=water|adhan|save-rainfall|garbage")
	flag.Parse()

	// Print commandline arguments
	logutil.Log("Time:\t\t", timeNow.Format("02-01-2006 15:04:05"))
	logutil.Log("Log Level:\t", +logutil.LogLevel)
	logutil.Log("Task:\t\t", *taskPtr, "\n")

	if *taskPtr == "adhan" {
		_, err := adhan.GetAdhan()
		if err != nil {
			logutil.Log("adhan timings failed. Error: " + err.Error())
			return
		}
		logutil.Log("adhan timings fetched successfully")
	}

	if *taskPtr == "water" {
		err := water.WaterLawn()
		if err != nil {
			logutil.Log("watering lawn failed. Error: " + err.Error())
			return
		}
		logutil.Log("watering lawn queued successfully")
	}

	// This is specific to my case
	// My analogue water tap does not have daylight savings
	// I watering to start at 8:30 or 9:30 depending upon daylight savings (my analogue tap time will turn on at 8:30)
	if *taskPtr == "water-at-0830" {
		isDSTTime := dateutil.IsDST()
		hour := timeNow.Format("15")

		logutil.Log(fmt.Sprintf("Daylight Savings? %t | Hour: %s", isDSTTime, hour))

		// If it is daylight savings and hour is 9AM OR No daylight savings and hour is 8AM
		if (isDSTTime && hour == "09") || (!isDSTTime && hour == "08") {
			err := water.WaterLawn()
			if err != nil {
				logutil.Log("watering lawn failed. Error: " + err.Error())
				return
			}
			logutil.Log("watering lawn queued successfully")
		} else {
			logutil.Log("watering lawn failed. It is not the right hour")
		}
	}

	if *taskPtr == "save-rainfall" {
		if appConfig.WeatherStationUrl != "" {
			logutil.Log("Saving today's rainfall is unnecessary when using weather station")
			return
		}

		err := water.SaveRainfall()
		if err != nil {
			logutil.Log("Saving today's rainfall failed. Error: " + err.Error())
			return
		}
		logutil.Log("Today's rainfall saved successfully")
	}

	if *taskPtr == "garbage" {
		err := garbagecollection.SaveGarbageCollection()
		if err != nil {
			logutil.Log("Garbage date saving failed. Error: " + err.Error())
			return
		}
		logutil.Log("Garbage dates saved successfully")
	}

	if *taskPtr == "help" {
		fmt.Printf("\nGoWaterLawn: Water your lawn with open sprinker \nApp version: " + appVersion + " \nFlags: \n  --task\t The task to run. Accepted values: adhan|water|water-at-0830|save-rainfall|garbage\n\n")
	}
	logutil.Log("------------------------------------\n")
}
