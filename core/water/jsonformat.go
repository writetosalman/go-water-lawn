package water

// https://stackoverflow.com/questions/36062052/unmarshal-generic-json-in-go
// Generic parser if needed

// JSON format structs for Willy Weather forecast
type location struct {
	Id       int     `json:"id"`
	Name     string  `json:"name"`
	Region   string  `json:"region"`
	State    string  `json:"state"`
	Postcode string  `json:"postcode"`
	TimeZone string  `json:"timeZone"`
	Lat      float64 `json:"lat"`
	Lng      float64 `json:"lng"`
	TypeId   int     `json:"typeId"`
}

type rainfallDayEntry struct {
	DateTime    string `json:"dateTime"`
	StartRange  int    `json:"startRange"`
	EndRange    int    `json:"endRange"`
	RangeDivide string `json:"rangeDivide"`
	RangeCode   string `json:"rangeCode"`
	Probability int    `json:"probability"`
}

type rainfallDay struct {
	DateTime string             `json:"dateTime"`
	Entries  []rainfallDayEntry `json:"entries"`
}

type rainfallUnit struct {
	Amount string `json:"amount"`
}

type rainfall struct {
	Days          []rainfallDay `json:"days"`
	Units         rainfallUnit  `json:"units"`
	IssueDateTime string        `json:"issueDateTime"`
}

type weatherDayEntry struct {
	DateTime          string `json:"dateTime"`
	PrecisCode        string `json:"precisCode"`
	Precis            string `json:"precis"`
	PrecisOverlayCode string `json:"precisOverlayCode"`
	Night             bool   `json:"night"`
	Min               int    `json:"min"`
	Max               int    `json:"max"`
}

type weatherDay struct {
	DateTime string            `json:"dateTime"`
	Entries  []weatherDayEntry `json:"entries"`
}

type weatherUnit struct {
	Temperature string `json:"temperature"`
}

type weather struct {
	Days          []weatherDay `json:"days"`
	Units         weatherUnit  `json:"units"`
	IssueDateTime string       `json:"issueDateTime"`
}

type forecasts struct {
	Rainfall rainfall `json:"rainfall"`
	Weather  weather  `json:"weather"`
}

type forecast struct {
	Location  location  `json:"location"`
	Forecasts forecasts `json:"forecasts"`
}

// JSON format struct to save historic data
type historicRainfall struct {
	Yesterday      float64 `json:"yesterday"`
	TwoDayBefore   float64 `json:"twoDayBefore"`
	ThreeDayBefore float64 `json:"threeDayBefore"`
}

type historicData struct {
	Rainfall     historicRainfall `json:"rainfall"`
	LastWaterDay string           `json:"lastWaterDay"`
}

// WeatherStat Weather Station json format
type WeatherStat struct {
	TempHigh  float64 `json:"tempHigh"`
	TempLow   float64 `json:"tempLow"`
	TempAvg   float64 `json:"tempAvg"`
	RainTotal float64 `json:"precipRate"`
}

type WeatherStatItem struct {
	StationId   string      `json:"stationID"`
	Tz          string      `json:"tz"`
	TimeLocal   string      `json:"obsTimeLocal"`
	TimeUtc     string      `json:"obsTimeUtc"`
	WeatherStat WeatherStat `json:"metric"`
}

type WeatherStatData struct {
	Status           string
	Code             string
	Total            int
	WeatherStatsItem []WeatherStatItem `json:"summaries"`
}
