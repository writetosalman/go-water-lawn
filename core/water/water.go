package water

import (
	"bitbucket.org/writetosalman/go-water-lawn/config"
	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
	"fmt"
	"time"
)

var AppConfig *config.Config
var weatherForecast forecast
var waterHistoricData historicData

// WaterLawn
// Get Water Lawn
func WaterLawn() error {
	// Rainfall forecast
	arrRainfall, arrWeatherDay, err := getForecast()
	if err != nil {
		return err
	}

	var rainfallEntry rainfallDayEntry
	var weatherEntry weatherDayEntry

	// Iterate rainfall forecast for next 3 days
	for index, day := range arrRainfall {
		// For each day go to entires
		rainfallEntry = day.Entries[0]
		rainfallEntry.DateTime = rainfallEntry.DateTime[0:10]
		logutil.InfoLog(fmt.Sprintf("%d: Rainfall for %s is %d - %d with probability %d [%s]", index, rainfallEntry.DateTime, rainfallEntry.StartRange, rainfallEntry.EndRange, rainfallEntry.Probability, rainfallEntry.RangeCode))

		if rainfallEntry.StartRange >= 5 && rainfallEntry.Probability > 35 {
			return fmt.Errorf("DONOT water lawn. Rain of %d - %d mm expected with probability %d on %s", rainfallEntry.StartRange, rainfallEntry.EndRange, rainfallEntry.Probability, rainfallEntry.DateTime)
		}
	}

	// Iterate weather forecast for today
	for index, day := range arrWeatherDay {
		// For each day go to entires
		weatherEntry = day.Entries[0]
		weatherEntry.DateTime = weatherEntry.DateTime[0:10]
		logutil.InfoLog(fmt.Sprintf("%d: Weather forecast for %s  is %d°C min - %d°C max [%s]", index, weatherEntry.DateTime, weatherEntry.Min, weatherEntry.Max, weatherEntry.Precis))

		break // Only today
	}

	// Get historical watering and rain data
	waterHistoricData, err := getHistoricData()
	if err != nil {
		return err
	}

	// Calculate past rainfall
	pastRainInMM := 0.0
	if AppConfig.WeatherStationUrl != "" {
		pastRainInMM, err = totalRainfallFromWeatherStation()

	} else {
		pastRainInMM = waterHistoricData.Rainfall.Yesterday + waterHistoricData.Rainfall.TwoDayBefore + waterHistoricData.Rainfall.ThreeDayBefore
	}
	logutil.InfoLog(fmt.Sprintf("Rainfall in last 3 days: %.1f mm", pastRainInMM))

	if pastRainInMM > 5 {
		return fmt.Errorf("DONOT water lawn. Rain of %.1f mm fell in past %d days", pastRainInMM, HISTORY_IN_DAYS)
	}

	// Check when was last watering date if weather is not extra hot
	if weatherEntry.Max < 34 {
		lastWaterDay, err := time.Parse("02-01-2006 15:04:05", waterHistoricData.LastWaterDay)
		if err != nil {
			return fmt.Errorf("failed to parse last water date. Error: %s", err.Error())
		}
		duration := time.Now().Sub(lastWaterDay)
		durationInDays := duration.Hours() / 24
		logutil.DebugLog(fmt.Sprintf("Last watering was done %.1f days ago", durationInDays))

		if durationInDays < 3 {
			return fmt.Errorf("DONOT water lawn as it was done %.1f days ago and max temp today will be %d", durationInDays, weatherEntry.Max)
		}
	}

	// Queue sprinklers
	err = queueSprinklers(waterHistoricData)
	if err != nil {
		return fmt.Errorf("failed to queue sprinklers. Error: " + err.Error())
	}
	return nil
}

// SaveRainfall
// This function updates history
// This function should run at 11:59PM
func SaveRainfall() error {

	// Find rain today
	rainfallToday, err := rainfallToday()
	if err != nil {
		return err
	}

	return saveInPastRainData(rainfallToday)
}
