package water

import (
	"fmt"

	"bitbucket.org/writetosalman/go-water-lawn/utility/conversionutil"

	"bitbucket.org/writetosalman/go-water-lawn/utility/fileutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/jsonutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
)

var HISTORY_IN_DAYS = 3

func getHistoricData() (historicData, error) {
	// Find rain in past
	content, err := fileutil.Read(AppConfig.WeatherDbFilename)
	if err != nil {
		return historicData{}, fmt.Errorf("failed to read weather history. Error: %s", err.Error())
	}

	err = jsonutil.Parse(content, &waterHistoricData)

	if err != nil {
		return historicData{}, fmt.Errorf("failed to parse weather history data. Error: %s", err.Error())
	}
	logutil.DebugLog(fmt.Sprintf("\nWeather history: %v", waterHistoricData))

	return waterHistoricData, nil
}

func saveInPastRainData(rainToday string) error {
	// Find rain in past
	content, err := fileutil.Read(AppConfig.WeatherDbFilename)
	if err != nil {
		return fmt.Errorf("failed to read weather history. Error: %s", err.Error())
	}

	err = jsonutil.Parse(content, &waterHistoricData)

	if err != nil {
		return fmt.Errorf("failed to parse weather history. Error: %s", err.Error())
	}
	logutil.DebugLog(fmt.Sprintf("\nWeather history: %v", waterHistoricData))

	// Move historic data
	waterHistoricData.Rainfall.ThreeDayBefore = waterHistoricData.Rainfall.TwoDayBefore
	waterHistoricData.Rainfall.TwoDayBefore = waterHistoricData.Rainfall.Yesterday
	waterHistoricData.Rainfall.Yesterday = conversionutil.StringToFloat64(rainToday)
	logutil.DebugLog(fmt.Sprintf("\nWeather history updated: %v", waterHistoricData))

	// Save historic data
	err = saveHistoryFile(waterHistoricData)
	if err != nil {
		return err
	}

	return nil
}
