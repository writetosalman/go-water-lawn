package water

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/writetosalman/go-water-lawn/service/jsonservice"
	"bitbucket.org/writetosalman/go-water-lawn/utility/fileutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/jsonutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/urlutil"
)

func getForecast() ([]rainfallDay, []weatherDay, error) {
	// Declare var so they remain in scope
	var content []byte
	var err error

	// Decide based on debug
	if AppConfig.Debug {
		content, err = fileutil.Read("willyweather-forecast.json")
	} else {
		content, err = urlutil.Get(AppConfig.WeatherForecastUrl)
	}

	// Check error
	if err != nil {
		return nil, nil, fmt.Errorf("failed to get forecast. Error: %s", err.Error())
	}

	err = jsonutil.Parse(content, &weatherForecast)

	if err != nil {
		return nil, nil, fmt.Errorf("failed to parse forecast. Error: %s", err.Error())
	}

	// Get days forecast
	return weatherForecast.Forecasts.Rainfall.Days, weatherForecast.Forecasts.Weather.Days, nil

	// // Iterate
	// for index, day := range arrRainfall {
	// 	// For each day go to entires
	// 	rainfallEntry = day.Entries[0]
	// 	rainfallEntry.DateTime = rainfallEntry.DateTime[0:10]
	// 	logutil.InfoLog(fmt.Sprintf("%d: Rainfall for %s is %d - %d with probability %d [%s]", index, rainfallEntry.DateTime, rainfallEntry.StartRange, rainfallEntry.EndRange, rainfallEntry.Probability, rainfallEntry.RangeCode))
	// }

	// // Iterate
	// for index, day := range arrWeather {
	// 	// For each day go to entires
	// 	weatherEntry = day.Entries[0]
	// 	weatherEntry.DateTime = weatherEntry.DateTime[0:10]
	// 	logutil.InfoLog(fmt.Sprintf("%d: Weather for %s is %d - %d [%s]", index, weatherEntry.DateTime, weatherEntry.Min, weatherEntry.Max, weatherEntry.PrecisCode))
	// }
}

func rainfallToday() (string, error) {
	// Declare var so they remain in scope
	var content []byte
	var err error

	// Decide based on debug
	if AppConfig.Debug {
		content, err = fileutil.Read("willyweather-richmond-station.html")
	} else {
		content, err = urlutil.Get(AppConfig.RainfallTodayUrl)
	}

	if err != nil {
		return "", fmt.Errorf("failed to parse today's rain. Error: %s", err.Error())
	}

	// Manipulate string to get rainfall today
	strContent := string(content)
	startPost := strings.Index(strContent, "id=\"gauge-rainfall\"")
	rainToday := strContent[(startPost + 170):(startPost + 170 + 62)]
	rainToday = strings.TrimSpace(strings.Replace(strings.Replace(rainToday, "Today", "", 1), "mm", "", 1))
	logutil.InfoLog("Rainfall today: " + rainToday + " mm")

	return rainToday, nil
}

func totalRainfallFromWeatherStation() (float64, error) {
	// Declare var so they remain in scope
	var err error
	var weatherStatData WeatherStatData
	var rainfall float64

	err = jsonservice.JsonGet(&weatherStatData, AppConfig.WeatherStationUrl)
	if err != nil {
		return 0, fmt.Errorf("failed to get weather station data. Error: %s", err.Error())
	}

	for index, weatherData := range weatherStatData.WeatherStatsItem {
		if index < 4 {
			continue
		}
		logutil.DebugLog(fmt.Sprintf("%d: Total rainfall at %s on %s is %.2f mm", index, weatherData.StationId, weatherData.TimeLocal[0:10], weatherData.WeatherStat.RainTotal))
		rainfall += weatherData.WeatherStat.RainTotal
	}

	return rainfall, nil
}

// queueSprinklers
// URL format to open sprinkler http://{IP}:8080/cm?pw={API_KEY}&sid={SPRINKLER_ID}&en={ON_OR_OFF}&t={SECONDS_IF_ON}
func queueSprinklers(data historicData) error {

	sprinklerList := strings.Split(AppConfig.SprinklerWaterList, "|")
	for index1, sprinkler := range sprinklerList {

		logutil.DebugLog(fmt.Sprintf("%d sprinkler list: %s", index1, sprinkler))
		sprinklerOptionList := strings.Split(sprinkler, ":")

		// Must have 3 options
		if len(sprinklerOptionList) == 3 {

			// Start Sprinkler
			_, err := urlutil.Get(AppConfig.OpenSprinklerUrl + "&sid=" + sprinklerOptionList[0] + "&en=1&t=" + sprinklerOptionList[1])
			if err != nil {
				return err
			}
			logutil.InfoLog(fmt.Sprintf("%s sprinkler queued for %s sec", sprinklerOptionList[2], sprinklerOptionList[1]))

			// Wait so that not all sprinklers start together
			time.Sleep(3 * time.Second)

		} else {
			logutil.Log(fmt.Sprintf("Error: cannot start sprinkler, it does not have all settings. %v", sprinklerOptionList))
		}
	}

	// Update history
	timeNow := time.Now()
	data.LastWaterDay = timeNow.Format("02-01-2006 15:04:05")

	// Save historic data
	err := saveHistoryFile(data)
	if err != nil {
		return err
	}

	// All good
	return nil
}

func saveHistoryFile(data historicData) error {
	jsonContent, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("failed to convert weather history for saving. Error: %s", err.Error())
	}

	err = fileutil.Write(AppConfig.WeatherDbFilename, jsonContent)
	if err != nil {
		return fmt.Errorf("failed to save weather history. Error: %s", err.Error())
	}

	return nil
}
