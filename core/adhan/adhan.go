package adhan

import (
	"bitbucket.org/writetosalman/go-water-lawn/config"
	"bitbucket.org/writetosalman/go-water-lawn/utility/fileutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/urlutil"
)

var AppConfig *config.Config

// Get Adhan
func GetAdhan() ([]byte, error) {
	content, err := urlutil.Get(AppConfig.AdhanUrl)

	// Got conent successfully
	if err == nil {
		// Update cache
		if err = fileutil.Write(AppConfig.AdhanFilename, content); err != nil {
			logutil.DebugLog(err.Error())
		}

		// Return data
		return content, nil
	}

	// Incase of error
	logutil.DebugLog(err.Error())
	content, err = fileutil.Read(AppConfig.AdhanFilename)

	// Return cache data
	if err == nil {
		return content, nil
	}

	return nil, err
}
