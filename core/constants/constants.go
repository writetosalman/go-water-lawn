package constants

const LogLevelQuite = 0
const LogLevelCritical = 1
const LogLevelInfo = 2
const LogLevelDebug = 3
const DateFormat = "02 Jan 2006"
const Tuesday1stWeekOfYear = "03 Jan 2023"
