package garbagecollection

import (
	"bitbucket.org/writetosalman/go-water-lawn/config"
	"bitbucket.org/writetosalman/go-water-lawn/core/constants"
	"bitbucket.org/writetosalman/go-water-lawn/utility/fileutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
	"encoding/json"
	"fmt"
	"math"
	"time"
)

var AppConfig *config.Config

type GarbageCollectionStat struct {
	GarbageDate   string `json:"garbageDate"`
	GarbageInDays int    `json:"garbageInDays"`
	RecycleDate   string `json:"recycleDate"`
	RecycleInDays int    `json:"recycleInDays"`
}

// GetGarbageCollectionDate Get Garbage Day
func GetGarbageCollectionDate() (time.Time, int, error) {
	todayNow := time.Now()
	today, _ := time.Parse(constants.DateFormat, todayNow.Format(constants.DateFormat))
	logutil.DebugLog("Today ", today)

	weekday := today.Weekday()
	weekdayNum := int(weekday)
	if weekdayNum == 2 {
		logutil.DebugLog("Garbage collection day is today")
		return today, 0, nil
	}

	// Today is before Tuesday
	if weekdayNum < 2 {
		numOfDays := 2 - weekdayNum
		garbageDay := today.AddDate(0, 0, numOfDays)
		logutil.DebugLog(fmt.Sprintf("\nGarbage collection day is %s in %d days this week", garbageDay.Format(constants.DateFormat), numOfDays))
		return garbageDay, numOfDays, nil
	}

	numOfDays := 7 - weekdayNum + 2 // 7 - Wednesday(3) + 2
	garbageDay := today.AddDate(0, 0, numOfDays)
	logutil.DebugLog(fmt.Sprintf("\nGarbage collection day is %s in %d days next week", garbageDay.Format(constants.DateFormat), numOfDays))
	return garbageDay, numOfDays, nil
}

// GetRecycleCollectionDate Get Recycle Collection Day
func GetRecycleCollectionDate() (time.Time, int, error) {
	// Get first tuesday of 2023
	tuesday1st, err := time.Parse(constants.DateFormat, constants.Tuesday1stWeekOfYear)
	logutil.DebugLog(fmt.Sprintf("\n-----------------\nRecycle collection debug\n1st Tuesday of year: %s", tuesday1st.Format(constants.DateFormat)))
	if err != nil {
		return time.Time{}, 0, err
	}

	// Get tuesday for garbage collection
	tuesday, numOfDays, err := GetGarbageCollectionDate()
	if err != nil {
		return time.Time{}, 0, err
	}

	duration := tuesday.Sub(tuesday1st)
	durationInHrs := duration.Hours()
	durationInWeeks := int(math.Ceil(durationInHrs/24/7)) + 1
	modulus := durationInWeeks % 2
	logutil.DebugLog(fmt.Sprintf("Even/Odd: %s - Tuesday: %s - Duration In hours: %.1f - In Weeks: %d\n", AppConfig.RecycleCollectionTuesday, tuesday.Format(constants.DateFormat), durationInHrs, durationInWeeks))

	// Incase no further calculation is needed
	if (AppConfig.RecycleCollectionTuesday == "even" && modulus == 0) || (AppConfig.RecycleCollectionTuesday == "odd" && modulus == 1) {
		logutil.DebugLog(fmt.Sprintf("Recycle collection day is %s in %d days\n-----------------", tuesday.Format(constants.DateFormat), numOfDays))
		return tuesday, numOfDays, nil
	}

	nextTuesday := tuesday.AddDate(0, 0, 7)
	nextNumOfDays := numOfDays + 7
	logutil.DebugLog(fmt.Sprintf("Recycle collection day is %s in %d days\n-----------------", nextTuesday.Format(constants.DateFormat), nextNumOfDays))
	return nextTuesday, nextNumOfDays, nil
}

func SaveGarbageCollection() error {
	// Get tuesday for garbage collection
	garbageTuesday, numOfDaysG, err := GetGarbageCollectionDate()
	if err != nil {
		return fmt.Errorf("failed to get garbage collection day. Error: %s", err.Error())
	}

	// Get tuesday for recycle collection
	recycleTuesday, numOfDaysR, err := GetRecycleCollectionDate()
	if err != nil {
		return fmt.Errorf("failed to get recyle collection day. Error: %s", err.Error())
	}

	jsonContent, err := json.Marshal(GarbageCollectionStat{
		garbageTuesday.Format(constants.DateFormat),
		numOfDaysG,
		recycleTuesday.Format(constants.DateFormat),
		numOfDaysR,
	})
	if err != nil {
		return fmt.Errorf("failed to convert garbage collection json for saving. Error: %s", err.Error())
	}

	err = fileutil.Write(AppConfig.GarbageFilename, jsonContent)
	if err != nil {
		return fmt.Errorf("failed to save garbage collection file. Error: %s", err.Error())
	}
	return nil
}
