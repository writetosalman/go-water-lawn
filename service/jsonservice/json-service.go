package jsonservice

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	"bitbucket.org/writetosalman/go-water-lawn/utility/conversionutil"
)

var jsonClient = &http.Client{Timeout: 20 * time.Second}

func JsonGet(pointerToTarget interface{}, url string) error {

	// Get URL
	r, err := jsonClient.Get(url)
	if err != nil {
		return errors.New("Error occurred while opening URL. " + err.Error())
	}

	// Do not forget to close connection
	defer r.Body.Close()

	// Decode the response and return
	return json.NewDecoder(r.Body).Decode(pointerToTarget)
}

func JsonPost(pointerToTarget interface{}, url string, postData interface{}) error {
	// Marshall post data into JSON
	resultBytes, _ := json.Marshal(postData)

	// POST data to http
	response, err := jsonClient.Post(url, "application/jsonutil", bytes.NewBuffer(resultBytes))

	// Check if error occurred
	if err != nil {
		return errors.New("HTTP post failed with error" + err.Error())
	}

	if response.StatusCode != 200 {
		return errors.New("HTTP post was not successful [Status: " + conversionutil.IntToString(response.StatusCode) + "]")
	}

	// Read response
	data, _ := ioutil.ReadAll(response.Body)

	// Unmarshall response
	return json.Unmarshal(data, &pointerToTarget)
}

func JsonParseString(data string, pointerToTarget interface{}) error {
	byteArr := conversionutil.StringToByteArray(data)
	return json.Unmarshal(byteArr, &pointerToTarget)
}

func JsonParse(byteArr []byte, pointerToTarget interface{}) error {
	return json.Unmarshal(byteArr, &pointerToTarget)
}

func Stringify(v interface{}) (string, error) {
	b, err := json.Marshal(v)
	return string(b), err
}
