# Go Water Lawn

### Overview

This lightweight job has 3 tasks. 

1. It saves daily rainfall at 11:59PM to its database for historic use

1. It fetches weather forecast and decide based on that to start sprinkler or not.

1. It caches adhan timings. If remoted endpoint is dead, it displays yesterday's results.

### Watering algorthim

1. If rainfall in past 3 days is over 5mm, postpone watering

1. If weather forecast for next 3 days is for rain of atleast 5mm with probability of over 40% for any day, postpone watering

1. TODO: if temperature is expected to be over 40C and forecast is for no rain today, water for 30min.


### Setup

* Now that you have checked out the source code, you need to setup go project

* Run command `go mod init` and then `go mod tidy`

* Copy `.env.example` as `.env`

* Edit `.env` and enter your information for the variables

### Environment variables needed

| Name                   | Status       | Description                                                                                                                                                                                                      |
|------------------------|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `WEATHER_FORECAST_URL` |              | Willyweather URL to fetch rain and weather forcast of your area                                                                                                                                                  |
| `WEATHER_STATION_URL`  |              | Weather station URL to download last x days of rainfall and temperature data                                                                                                                                     |
| `RAINFALL_TODAY_URL`   | *deprecated* | Willweather URL to today's rainfall recorded by your nearest weather station                                                                                                                                     |
| `OPEN_SPRINKLER_URL`   |              | Open sprinkler URL to start your local sprinklers                                                                                                                                                                |
| `SPRINKLER_WATER_LIST` |              | PIPE (&#124;) delimited list of sprinklers to start, each sprinkler entry written as `<station number>:<duration in seconds>:<station name>` Example `2:2400:Backyard&#124;1:1800:Marraya&#124;0:1200:Frontyard` |
| `ADHAN_URL`            |              | Remote adhan timing URL that you want to cache                                                                                                                                                                   |


### Build

Build for Raspberry PI

* First checkout which OS you are running by command `cat /etc/os-release`

* Second, checkout which processor you have by running command `uname -a`

* You can get list of all support OS/Arch in go by running command `go tool dist list`

* Adjust following command to your needs. Make sure you zip the file before transferring.

```
env GOOS=linux GOARCH=arm go build -o go-water-lawn-pi && zip go-water-lawn-pi.zip go-water-lawn-pi
```

### Cronjobs

Setup following cronjobs

```
# min   hour    day     month   weekday command
0       4       *       *       *       cd /config/go-water-lawn && ./go.sh adhan
58     11       *       *       *       cd /config/go-water-lawn && ./go.sh save-rainfall
30     8,9      *       *       *       cd /config/go-water-lawn && ./go.sh water-at-0830

```

### Notes

For rainfall today, we can also use following links (last two display rainfall since 9AM)

- [WillyWeather Weather Station page](https://www.willyweather.com.au/climate/weather-stations/356/observations.html?code=067105&climate-context=weather-stations)

- http://www.bom.gov.au/products/IDN60801/IDN60801.95753.shtml#other_formats

- http://www.bom.gov.au/fwo/IDN60801/IDN60801.95753.json

-

