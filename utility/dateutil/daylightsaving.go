package dateutil

import (
	"time"
)

// https://stackoverflow.com/questions/53046636/how-to-check-whether-current-local-time-is-dst
// isTimeDST returns true if time t occurs within daylight saving time
// for its time zone.
func IsDSTFromTimeStamp(t time.Time) bool {
	// If the most recent (within the last year) clock change
	// was forward then assume the change was from std to dst.
	hh, mm, _ := t.UTC().Clock()
	tClock := hh*60 + mm
	for m := -1; m > -12; m-- {
		// assume dst lasts for least one month
		hh, mm, _ := t.AddDate(0, m, 0).UTC().Clock()
		clock := hh*60 + mm
		if clock != tClock {
			if clock > tClock {
				// std to dst
				return true
			}
			// dst to std
			return false
		}
	}
	// assume no dst
	return false
}

func IsDST() bool {
	return IsDSTFromTimeStamp(time.Now())
}
