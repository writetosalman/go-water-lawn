package compressutil

import (
	"bitbucket.org/writetosalman/go-water-lawn/utility/fileutil"
	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"runtime"
)

func Gunzip(source string, destination string) error {
	if source == "" {
		return errors.New("failed to gunzip file. source name is empty")
	}
	if err := fileutil.Exists(source); err != nil {
		return errors.New("failed to gunzip file. file does not exist")
	}

	if runtime.GOOS == "windows" {
		return errors.New("be real! run on linux or mac. quitting on windows")
	}

	// Delete old destination file
	if err := fileutil.Delete(destination); err != nil {
		logutil.DebugLog("error while deleting old uncompressed file. Error: " + err.Error())
	}

	cmd := exec.Command("gunzip", source)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()

	if err != nil {
		return fmt.Errorf("failed to unzip gz file Error: %s", err)
	}

	sourceWithoutExtension := fileutil.FileNameWithoutExtension(source)

	err = os.Rename(sourceWithoutExtension, destination)
	if err != nil {
		return fmt.Errorf("failed to rename file [%s] to [%s] during gunzip. Error: %s", sourceWithoutExtension, source, err.Error())
	}

	return nil
}
