package fileutil

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// This function writes contents of file
func Write(filename string, data []byte) error {

	if filename == "" {
		return errors.New("cannot write file. filename is empty")
	}

	err := ioutil.WriteFile(filename, data, 0644)
	if err != nil {
		return errors.New("unable to write file")
	}

	return nil
}

// This function writes contents of file
func Append(filename string, data []byte) error {

	if filename == "" {
		return errors.New("cannot write file. filename is empty")
	}

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return errors.New("unable to open file for writing. Error: " + err.Error())
	}

	if _, err := f.Write(data); err != nil {
		return errors.New("unable to write to file. Error: " + err.Error())
	}

	if err := f.Close(); err != nil {
		return errors.New("unable to close file. Error: " + err.Error())
	}

	return nil
}

// This function reads contents of file
func Read(filename string) ([]byte, error) {

	if filename == "" {
		return nil, errors.New("cannot read file. filename is empty")
	}

	if err := Exists(filename); err != nil {
		return nil, errors.New("cannot read file. it does not exists")
	}

	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, errors.New("unable to read file")
	}

	return data, nil
}

// This function checks if file exists
func Exists(filename string) error {

	if filename == "" {
		return errors.New("cannot check if file exists. filename is empty")
	}

	_, err := os.Stat(filename)

	// If error is not nil and it is of type file does not exists, return failure
	if err != nil {
		if os.IsExist(err) {
			return nil
		}
		return errors.New("file does not exists. Error: " + err.Error())
	}

	return nil
}

func FileNameWithoutExtension(fileName string) string {
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}

// This function reads contents of file
func Delete(filename string) error {

	if filename == "" {
		return errors.New("cannot delete file. filename is empty")
	}

	// if file does not exists, nothing to delete, return nil/success
	if err := Exists(filename); err != nil {
		return nil
	}

	err := os.Remove(filename)
	if err != nil {
		return errors.New("unable to delete file. Error: " + err.Error())
	}

	return nil
}
