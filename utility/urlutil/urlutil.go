package urlutil

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
)

func Download(url string, destination string) error {
	if url == "" {
		return errors.New("failed to download. URL is empty")
	}

	out, err := os.Create(destination)
	if err != nil {
		return fmt.Errorf("failed to download. open local file filed. Error: %s", err.Error())
	}
	defer out.Close()

	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to download. URL error. Error: %s", err.Error())
	}
	defer resp.Body.Close()

	// If HTTP status code is not within 200s
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return fmt.Errorf("failed to download. Http status code: %d", resp.StatusCode)
	}

	bytesRead, err := io.Copy(out, resp.Body)
	if err != nil {
		return fmt.Errorf("failed to download. URL read error. Error: %s", err.Error())
	}

	logutil.DebugLog(fmt.Sprintf("downloaded file of size %.2f", float64(bytesRead)/1048576))

	return nil
}

func Get(url string) ([]byte, error) {
	if url == "" {
		return nil, errors.New("failed to read url. URL is empty")
	}

	logutil.DebugLog(fmt.Sprintf("opening URL %s", url))

	// HTTP Client
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)

	// Add Request header
	req.Header.Set("Accept-Language", "en-US,en;q=0.9")
	req.Header.Set("Cache-Control", "max-age=0")
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("Upgrade-Insecure-Requests", "1")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36")
	//	req.Header.Set("Cookie", "__cfduid=d3e86fe2c893ed168d5cc4c4c5fef9fe31602836375");
	// req.Header.Set("Host", "api.aladhan.com")

	// Make Request
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to read url. URL error. Error: %s", err.Error())
	}
	defer resp.Body.Close()

	// If HTTP status code is not within 200s
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return nil, fmt.Errorf("failed to read url. Http status code: %d", resp.StatusCode)
	}

	// read all response body
	content, err := ioutil.ReadAll(resp.Body)

	// close response body
	resp.Body.Close()

	if err != nil {
		return nil, fmt.Errorf("failed to read url. Error: %s", err.Error())
	}

	logutil.DebugLog(fmt.Sprintf("downloaded file of size %.2f KB", float64(len(content))/1024))

	return content, nil
}
