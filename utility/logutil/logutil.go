package logutil

import (
	"fmt"
	"log"

	"bitbucket.org/writetosalman/go-water-lawn/core/constants"
)

var LogLevel = 0

// Log function writes log messages to the print
func Log(str ...interface{}) {
	fmt.Println(str...)
}

func CritialLog(str ...interface{}) {
	if LogLevel >= constants.LogLevelCritical {
		Log(str...)
	}
}

func InfoLog(str ...interface{}) {
	if LogLevel >= constants.LogLevelInfo {
		Log(str...)
	}
}

func DebugLog(str ...interface{}) {
	if LogLevel >= constants.LogLevelDebug {
		Log(str...)
	}
}

// CheckError function checks if there is error or not
func CheckError(err error) {
	if err != nil {
		Log("Error: " + err.Error())
		log.Fatal("Error: " + err.Error())
	}
}
