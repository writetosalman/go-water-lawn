package dynamicaccessutil

import (
	"bitbucket.org/writetosalman/go-water-lawn/utility/conversionutil"
	"errors"
	"fmt"
	"reflect"
)

// https://stackoverflow.com/questions/47961245/golang-dynamic-access-to-a-struct-property
// https://godoc.org/reflect#Value
// Usage: err := setField(&v, keyName, keyValue)
func SetField(v interface{}, name string, value string) error {
	rv, err := GetField(v)
	if err != nil {
		return err
	}

	fv := rv.FieldByName(name)
	if !fv.IsValid() {
		return fmt.Errorf("not a field name: %s", name)
	}
	if !fv.CanSet() {
		return fmt.Errorf("cannot set field %s", name)
	}
	if fv.Kind() != reflect.String {
		return fmt.Errorf("%s is not a string field", name)
	}
	fv.SetString(value)
	return nil
}

func GetField(v interface{}) (reflect.Value, error) {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Ptr || rv.Elem().Kind() != reflect.Struct {
		return rv, errors.New("GetField requires a pointer to struct")
	}
	rv = rv.Elem()
	return rv, nil
}

func GetStringFromValue(rv reflect.Value, name string) (string, error) {
	fv := rv.FieldByName(name)
	if !fv.IsValid() {
		return "", fmt.Errorf("not a field name: %s", name)
	}
	if fv.Kind() != reflect.String {
		return "", fmt.Errorf("%s is not a string field", name)
	}

	return fv.String(), nil
}

func GetIntFromValue(rv reflect.Value, name string) (int, error) {
	fv := rv.FieldByName(name)
	if !fv.IsValid() {
		return 0, fmt.Errorf("not a field name: %s", name)
	}
	if fv.Kind() != reflect.Int && fv.Kind() != reflect.String {
		return 0, fmt.Errorf("%s is not a string|int field", name)
	}

	if fv.Kind() == reflect.String {
		return conversionutil.StringToInt(fv.String()), nil
	}
	return int(fv.Int()), nil
}

func GetFloatFromValue(rv reflect.Value, name string) (float64, error) {
	fv := rv.FieldByName(name)
	if !fv.IsValid() {
		return 0, fmt.Errorf("not a field name: %s", name)
	}
	if fv.Kind() != reflect.Float32 && fv.Kind() != reflect.Float64 && fv.Kind() != reflect.String {
		return 0, fmt.Errorf("%s is not a string|float field", name)
	}

	if fv.Kind() == reflect.String {
		return conversionutil.StringToFloat64(fv.String()), nil
	}
	return fv.Float(), nil
}

func GetString(v interface{}, name string) (string, error) {
	rv, err := GetField(v)
	if err != nil {
		return "", err
	}

	return GetStringFromValue(rv, name)
}

func GetInt(v interface{}, name string) (int, error) {
	rv, err := GetField(v)
	if err != nil {
		return 0, err
	}

	return GetIntFromValue(rv, name)
}
