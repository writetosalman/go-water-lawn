package jsonutil

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/writetosalman/go-water-lawn/utility/conversionutil"
)

type JsonResponse struct {
	Data string `json:"data"`
}

func SendResponseHeaders(w http.ResponseWriter, httpStatus int) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET,POST,OPTIONS")
	w.WriteHeader(httpStatus)
}

// JsonResponse sends out JSON to the browser
func Response(response string, w http.ResponseWriter, httpStatus int) {
	SendResponseHeaders(w, httpStatus)
	ResponseArray(JsonResponse{response}, w, httpStatus)
}

// Json Response sents out JSON array to the browser
func ResponseArray(response interface{}, w http.ResponseWriter, httpStatus int) {

	jsonParsed, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/jsonutil")
	SendResponseHeaders(w, httpStatus)
	w.Write(jsonParsed)
}

// JsonResponse sends out JSON to the browser
func ResponseParsed(response interface{}, w http.ResponseWriter, httpStatus int) {

	w.Header().Set("Content-Type", "application/jsonutil")
	SendResponseHeaders(w, httpStatus)
	json.NewEncoder(w).Encode(response)
}

func ParseString(data string, pointerToTarget interface{}) error {
	byteArr := conversionutil.StringToByteArray(data)
	return json.Unmarshal(byteArr, &pointerToTarget)
}

func Parse(byteArr []byte, pointerToTarget interface{}) error {
	return json.Unmarshal(byteArr, &pointerToTarget)
}

func Stringify(v interface{}) (string, error) {
	b, err := json.Marshal(v)
	return string(b), err
}
