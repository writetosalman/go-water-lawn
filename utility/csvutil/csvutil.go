package csvutil

import (
	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
	"encoding/csv"
	"fmt"
	"os"
	"strings"
)

func readFile(csvFilename string, delimiter rune) ([][]string, error) {
	csvFile, err := os.Open(csvFilename)
	if err != nil {
		return nil, fmt.Errorf("failed to open CSV file. Error: %s", err.Error())
	}

	defer csvFile.Close()

	csvFileReader := csv.NewReader(csvFile)
	csvFileReader.Comma = delimiter

	csvData, err := csvFileReader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("failed to read CSV file. possible EOF ? Error: %s", err.Error())
	}

	return csvData, nil
}

func CSVFile(csvFilename string) ([][]string, error) {
	return readFile(csvFilename, ',')
}

func TSVFile(csvFilename string) ([][]string, error) {
	return readFile(csvFilename, '\t')
}

func GetFileHandler(filename string, delimiter rune) (*os.File, *csv.Reader, error) {

	csvFile, err := os.Open(filename)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to open CSV file for reading. Error: %s", err.Error())
	}

	csvFileReader := csv.NewReader(csvFile)
	csvFileReader.Comma = delimiter

	// Disable this, we dont need to check number of columns
	// https://godoc.org/encoding/csv#Reader.FieldsPerRecord
	csvFileReader.FieldsPerRecord = -1
	csvFileReader.LazyQuotes = true

	return csvFile, csvFileReader, nil
}

func GetValueForCSVArraySafely(csvData []string, index int) (string, error) {
	if index >= len(csvData) {
		errMsg := fmt.Sprintf("failed to get value | Index: %d | CSV Array Length: %d", index, len(csvData))
		logutil.DebugLog(fmt.Sprintf("------------- \n%s\nError: array out of bounds\nCSV Dump: \n%v \n------------- \n", errMsg, csvData))
		return "", fmt.Errorf(errMsg)
	}

	return csvData[index], nil
}

func GetDataSafeForCSVColumn(str string) string {
	str = strings.ReplaceAll(str, "\n", "")
	str = strings.ReplaceAll(str, "\"", "\\\"")
	return "\"" + str + "\""
}
