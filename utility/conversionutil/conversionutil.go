package conversionutil

import (
	"fmt"
	"strconv"
	"strings"
)

// StringArrayToInterface converts string array to interface array
func StringArrayToInterface(arrString []string) []interface{} {

	// Convert String array To Interface Array | https://golang.org/doc/faq#convert_slice_of_interface
	arrInterface := make([]interface{}, len(arrString))
	for index, item := range arrString {
		arrInterface[index] = item
	}

	return arrInterface
}

func StringListToIntList(str string) []int {
	if str != "" {
		arr := strings.Split(str, ",")
		ret := make([]int, len(arr))
		for index, element := range arr {
			ret[index] = StringToInt(element)
		}
		return ret
	}
	return nil
}

// Convert string to integer
func StringToInt(num string) int {

	if num == "" {
		return 0
	}

	number, err := strconv.Atoi(num)
	if err != nil {
		//logutil.Log("StringToInt Failed. " + err.Error())
		return 0
	}

	return number
}

// Convert string to integer64
func StringToInt64(num string) int64 {

	if num == "" {
		return 0
	}

	number, err := strconv.ParseInt(num, 10, 0)
	if err != nil {
		//logutil.Log("StringToInt64 Failed. " + err.Error())
		return 0
	}

	return number
}

// Convert string to float64
func StringToFloat64(num string) float64 {

	if num == "" {
		return float64(0)
	}

	number, err := strconv.ParseFloat(num, 0)
	if err != nil {
		//logutil.Log("StringToFloat64 Failed. " + err.Error())
		return 0
	}

	return number
}

// Convert integer to string
func IntToString(num int) string {

	if num == 0 {
		return "0"
	}

	strNumber := strconv.FormatInt(int64(num), 10)
	return strNumber
}

// Convert integer64 to string
func Int64ToString(num int64) string {

	if num == 0 {
		return "0"
	}

	strNumber := strconv.FormatInt(num, 10)
	return strNumber
}

// Convert float to string
func FloatToString(num float64) string {

	if num == 0 {
		return "0"
	}

	strNumber := strconv.FormatFloat(num, 'f', 10, 64)
	return strNumber
}

// Convert integer to string | https://stackoverflow.com/questions/27137521/how-to-convert-interface-to-string
func InterfaceToString(str interface{}) string {

	if str == nil {
		return ""
	}

	return fmt.Sprintf("%v", str)

	/*
		// Assert if interface{} is string | https://stackoverflow.com/questions/14289256/cannot-convert-data-type-interface-to-type-string-need-type-assertion
		strEmail, isOk 	:= arrTokenClaims["email"].(string)
		if !isOk {
			return map[string]string{"error":"Error parsing token email"}, false
		} */
}

func StringToByteArray(str string) []byte {
	return []byte(str)
}

func IntToInt64(i int) int64 {
	return int64(i)
}

func Int64ToInt(i int64) int {
	return int(i)
}

//func InterfaceToStringMap(arr map[string]interface{}) map[string]string {
//
//	arrString := make(map[string]string, len(arr))
//	for index, item := range arr {
//		arrString[index] = item
//		fmt.Println("Index: " +index)
//		fmt.Println("Item: " +item)
//	}
//	return arrString
//}
