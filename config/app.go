package config

import (
	"os"

	"bitbucket.org/writetosalman/go-water-lawn/utility/logutil"
	"github.com/subosito/gotenv"
)

// This function initialises ENV file
func Initialise() {
	gotenv.Load()
}

// This function returns environment variables
func Getenv(key string) string {

	envVar := os.Getenv(key)
	if envVar != "" {
		return envVar
	}

	// Fatal error
	logutil.Log("Critical error. Environment variable [" + key + "] not found")
	os.Exit(500)
	return ""
}
