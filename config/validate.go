package config

import (
	"errors"

	"bitbucket.org/writetosalman/go-water-lawn/utility/conversionutil"
)

type Config struct {
	AdhanUrl                 string
	AdhanFilename            string
	WeatherForecastUrl       string
	WeatherStationUrl        string
	WeatherDbFilename        string
	RainfallTodayUrl         string
	OpenSprinklerUrl         string
	SprinklerWaterList       string
	RecycleCollectionTuesday string
	GarbageFilename          string
	LogLevel                 int
	Debug                    bool
	DoSendEmailReport        bool
	EmailTo                  string
	EmailCC                  string
	EmailFrom                string
	EmailSubject             string
	EmailSMTPServer          string
	EmailSMTPPort            string
	EmailSMTPUsername        string
	EmailSMTPPassword        string
}

// This function validates if all config are correct and returns config
func GetConfig() (Config, error) {

	adhanUrl := Getenv("ADHAN_URL")
	if adhanUrl == "" {
		return Config{}, errors.New("configuration error. Adhan url is empty")
	}

	adhanFile := Getenv("ADHAN_FILE")
	if adhanFile == "" {
		return Config{}, errors.New("configuration error. Adhan filename is empty")
	}

	weatherDbFile := Getenv("WEATHER_DB_FILE")
	if weatherDbFile == "" {
		return Config{}, errors.New("configuration error. Weather db filename is empty")
	}

	weatherForecastUrl := Getenv("WEATHER_FORECAST_URL")
	if weatherForecastUrl == "" {
		return Config{}, errors.New("configuration error. Weather forecast url is empty")
	}

	rainfallTodayUrl := Getenv("RAINFALL_TODAY_URL")
	if rainfallTodayUrl == "" {
		return Config{}, errors.New("configuration error. Rainfall today url is empty")
	}

	openSprinklerUrl := Getenv("OPEN_SPRINKLER_URL")
	if openSprinklerUrl == "" {
		return Config{}, errors.New("configuration error. Opensprinkler url is empty")
	}

	sprinklerWaterList := Getenv("SPRINKLER_WATER_LIST")
	if openSprinklerUrl == "" {
		return Config{}, errors.New("configuration error. Sprinkler watering list is empty")
	}

	weatherStationUrl := Getenv("WEATHER_STATION_URL")
	if weatherStationUrl == "" {
		return Config{}, errors.New("configuration error. Weather station Url is empty")
	}

	garbageFilename := Getenv("GARBAGE_DB_FILE")
	if garbageFilename == "" {
		garbageFilename = "garbage.json"
	}

	recycleCollectionTuesday := Getenv("RECYCLE_COLLECTION_TUESDAY")
	if recycleCollectionTuesday == "" {
		recycleCollectionTuesday = "even"
	}

	logLevel := Getenv("LOG_LEVEL")
	if logLevel == "" {
		logLevel = "1"
	}

	debug := false
	if Getenv("DEBUG") == "true" {
		debug = true
	}

	// Check email constants
	var emailTo, emailCC, emailFrom, emailSubject, emailSMTPServer, emailSMTPPort, emailSMTPUsername, emailSMTPPassword string

	doSendEmailReport := false
	if Getenv("SEND_EMAIL_REPORT") == "true" {
		doSendEmailReport = true
	}

	// If email config is set as true than check for other values also
	if doSendEmailReport {
		emailTo = Getenv("EMAIL_TO")
		if emailTo == "" {
			return Config{}, errors.New("configuration error. email-To address empty")
		}

		emailFrom = Getenv("EMAIL_FROM")
		if emailFrom == "" {
			return Config{}, errors.New("configuration error. email-From address empty")
		}

		emailSubject = Getenv("EMAIL_SUBJECT")
		if emailSubject == "" {
			return Config{}, errors.New("configuration error. email Subject address empty")
		}

		emailSMTPServer = Getenv("EMAIL_SMTP_SERVER")
		if emailSMTPServer == "" {
			return Config{}, errors.New("configuration error. email SMTP server empty")
		}

		emailSMTPPort = Getenv("EMAIL_SMTP_PORT")
		if emailSMTPPort == "" {
			return Config{}, errors.New("configuration error. email SMTP port empty")
		}

		emailCC = Getenv("EMAIL_CC")
		emailSMTPUsername = Getenv("EMAIL_SMTP_USERNAME")
		emailSMTPPassword = Getenv("EMAIL_SMTP_PASSWORD")
	}

	return Config{
		AdhanUrl:                 adhanUrl,
		AdhanFilename:            adhanFile,
		WeatherDbFilename:        weatherDbFile,
		WeatherForecastUrl:       weatherForecastUrl,
		WeatherStationUrl:        weatherStationUrl,
		RainfallTodayUrl:         rainfallTodayUrl,
		OpenSprinklerUrl:         openSprinklerUrl,
		SprinklerWaterList:       sprinklerWaterList,
		RecycleCollectionTuesday: recycleCollectionTuesday,
		GarbageFilename:          garbageFilename,
		LogLevel:                 conversionutil.StringToInt(logLevel),
		Debug:                    debug,
		DoSendEmailReport:        doSendEmailReport,
		EmailTo:                  emailTo,
		EmailCC:                  emailCC,
		EmailFrom:                emailFrom,
		EmailSubject:             emailSubject,
		EmailSMTPServer:          emailSMTPServer,
		EmailSMTPPort:            emailSMTPPort,
		EmailSMTPUsername:        emailSMTPUsername,
		EmailSMTPPassword:        emailSMTPPassword,
	}, nil
}
