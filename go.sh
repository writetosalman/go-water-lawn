#!/bin/bash
# #####################################################################
# Name: Go Water Lawn Run Shell Job
# Description: This job runs go-water-lawn cronjob
# Copyright: Salman Ahmed
# #####################################################################
#
# Usage for this job:
# autofeed.sh
# -- Output in console
set -euo pipefail
IFS=$'\n\t'

if [[ $# -eq 0 ]] ; then
    echo "First argument is missing. Please provide task. Accepted values: adhan|water|water-at-0830|save-rainfall|garbage"
    exit 1
fi


# SAVE VALUES
TASK_NAME=${1:-'adhan'}
OUTPUT_TO_CONSOLE=${3:-0}
LOG_FILENAME="log-$TASK_NAME.txt"

# START AUTOFEED COMMAND
printf 'Welcome to go water lawn cronjob.\n'
printf 'Task name:\t\t%s\n' "$TASK_NAME"
printf 'Log file name:\t\t%s\n' "$LOG_FILENAME"
printf 'Output to console?\t%d\n' "$OUTPUT_TO_CONSOLE"


# MOVE one folder up
printf 'Current directory: \t%s\n' "$(pwd)"

# START JOB
printf 'Starting job\n'
./go-water-lawn-pi --task="$TASK_NAME"  > "$LOG_FILENAME"

if [[ $OUTPUT_TO_CONSOLE -eq 1 ]] ; then
    if [[ -e "$LOG_FILENAME" ]] ; then
      cat "$LOG_FILENAME"
    else
      printf 'Log file does not exists\n'
    fi
fi

# DONE
printf 'Finished \n\n'
